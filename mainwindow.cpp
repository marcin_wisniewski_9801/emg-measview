#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    connect(ui->actionRun, &QAction::triggered, this, &MainWindow::sendCommand);
    connect(&serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(&serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(error(QSerialPort::SerialPortError)));


    ui->plot->setSeries(1, DSIZE2);
    ui->plot->plotMode=Plot::LinearPlot;
    ui->plot->plotColor[0]=Qt::red;
    ui->plot->setRange(0, DSIZE2-1, -1, 1);
    ui->plot->setAxes(10, 0,  1000*DSIZE2/FS, 10, -1, 1);


    ui->plot_2->setSeries(2, DSIZE2);
    ui->plot_2->plotMode=Plot::LinearPlot;
    ui->plot_2->plotColor[0]=Qt::green;
    ui->plot_2->setRange(0, DSIZE2-1, 0, 1);
    ui->plot_2->setAxes(10, 0,  1000*DSIZE2/FS, 10, 0, 1);

    ui->plot_3->setSeries(2, DSIZE2);
    ui->plot_3->plotMode=Plot::LinearPlot;
    ui->plot_3->plotColor[1]=Qt::white;
    ui->plot_3->setRange(0, DSIZE2-1, 0, 1);
    ui->plot_3->setAxes(10, 0,  1000*DSIZE2/FS, 10, 0, 1);

    mask.resize(MSIZE);
    mask.fill(1.0/MSIZE);

    ui->statusbar->showMessage("No device");
    QString portname;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        if (info.description()=="Urządzenie szeregowe USB") {
            portname=info.portName();
            serial.setPortName(portname);
            if (serial.open(QIODevice::ReadWrite)) {
                ui->statusbar->showMessage(tr("Device: %1").arg(info.description()));
                serial.clear();
                ui->statusbar->setEnabled(true);
            } else {
                ui->statusbar->showMessage(tr("Can't open %1, error code %2") .arg(serial.portName()).arg(serial.error()));
                return;
            }
            break;
        }
    }
}

MainWindow::~MainWindow()
{
    serial.close();
    delete ui;
}

void MainWindow::sendCommand()
{
    senddata.clear();
    senddata.resize(1);
    senddata[0]=static_cast<uint8_t>(ui->actionRun->isChecked() << 7); // Run/Stop
    serial.write(senddata);
}

void MainWindow::readData()
{
    static uint32_t fr=0;
    static uint32_t count=(MSIZE/2)+1;

    if (serial.size() >= RSIZE) {

        readdata=serial.read(RSIZE);
        qDebug()<<readdata.size();

        uint16_t *sample=reinterpret_cast<uint16_t*>(readdata.data());

        // remove DC offset
        double dc=0;
        for (int n=0; n<RSIZE2; n++) {
            dc+=(sample[n]-32768)/32768.0;
        }
        dc/=RSIZE2;

        for (int n=0; n<RSIZE2; n++) {
            // Raw data - DC offset
            ui->plot->dataSeries[0][fr+n]= ((sample[n]-32768)/32768.0)-dc;


            // example - other plots
            ui->plot_2->dataSeries[0][fr+n]=abs(ui->plot->dataSeries[0][fr+n]);

        }
        static uint32_t temp=0;
        if(fr==0){
            for(int i=0;i<RSIZE2;i++){
                    for(int j=0;j<MSIZE;j++){
                        average+=ui->plot_2->dataSeries[0][fr+i+j]*mask[j];
                    }

                    ui->plot_3->dataSeries[1][count]=average;
                    average=0.0;
                    qDebug()<<count;

                count++;
                if(count >=DSIZE2-(MSIZE/2))
                    count=(MSIZE/2);
           }
            temp+=RSIZE2-MSIZE;
       }else{
            for(int i=0;i<RSIZE2;i++){
                if(fr+i<=DSIZE2-MSIZE){
                    for(int j=0;j<MSIZE;j++){
                        average+=ui->plot_2->dataSeries[0][temp+i+j]*mask[j];

                    }

                    ui->plot_3->dataSeries[1][count]=average;
                    average=0.0;
                    qDebug()<<count;
                }
                count++;
                if(count >=DSIZE2)
                    count=0;
            }
            temp+=RSIZE2;
        }
        if(temp>=DSIZE2-MSIZE)
            temp=0;

        fr+=RSIZE2;
        if(fr>=DSIZE2)
            fr=0;

        ui->plot->update();
        ui->plot_2->update();
        ui->plot_3->update();
    }
}

void MainWindow::error(QSerialPort::SerialPortError error)
{
    qDebug()<<error;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    if(ui->actionRun->isChecked())
        ui->actionRun->trigger();
}

void MainWindow::calculateFFT()
{
    fftData.fill(0);
    for(int i=0;i<DSIZE2; i++)
        fftData[static_cast<uint>(i)].real(ui->plot->dataSeries[0][i]*fftWin[i]);

    fftData=arma::fft(fftData);

    for(int i=0;i<FFT_SIZE2; i++) {
        magnitudeData[i]=abs(fftData[static_cast<uint>(i)])/(FFT_SIZE2);
        phaseData[i]=arg(fftData[static_cast<uint>(i)]);
    }
}


