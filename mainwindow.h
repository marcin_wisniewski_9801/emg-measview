#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <math.h>
#include <armadillo>

#define FS          2048

#define MSIZE       128
#define RSIZE       512
#define RSIZE2      (RSIZE/2)

#define DSIZE       (RSIZE*100)
#define DSIZE2      (DSIZE/2)

#define FFT_SIZE    2048
#define FFT_SIZE2   (FFT_SIZE/2)

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void sendCommand();
    void readData();
    void error(QSerialPort::SerialPortError error);
    void closeEvent(QCloseEvent *event);



private:
    Ui::MainWindow *ui;
     void calculateFFT();

    QSerialPort serial;
    QByteArray senddata;
    QByteArray readdata;
    double average;
    QVector<double> magnitudeData;
    QVector<double> phaseData;
    QVector<double> fftWin;
    QVector<double> mask;

    arma::cx_vec fftData;

};
#endif // MAINWINDOW_H
